import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

import 'package:path_provider/path_provider.dart';

//***************************** MAIN APP ***************************************
void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

//******************************* CLASS MAIN ***********************************
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

//******************************** LAYOUT MAIN *********************************
class _HomeState extends State<Home> {
  //VARIABLE
  List _toDoList = [];

  Map<String, dynamic> _lastRemoved;
  int _lastRemovedPos;

  final _toDoController = TextEditingController();

  //:::::::::::::::: METHOD CALL ALL TIME WHEN INIT _HOME STATE ::::::::::::::::
  @override
  void initState() {
    super.initState();

    //THEN -> CALL FUNCTION WHEN END _readData
    _readData().then((data) {
      setState(() {
        _toDoList = json.decode(data);
      });
    });
  }

  //::::::::::::::::::::::::: FUTURE FOR CREATE PATH :::::::::::::::::::::::::::
  Future<File> _getFile() async {
    //wait for directory
    final directory = await getApplicationDocumentsDirectory();

    return File("${directory.path}/data.json");
  }

  //::::::::::::::::::::::::: FUTURE FOR SAVE PATH :::::::::::::::::::::::::::::
  Future<File> _saveData() async {
    String data = json.encode(_toDoList); //TRANSFORM LIST TO JSON

    final file = await _getFile();
    return file.writeAsString(data);
  }

  //::::::::::::::::::::::::: FUTURE FOR READ PATH :::::::::::::::::::::::::::::
  Future<String> _readData() async {
    try {
      final file = await _getFile();

      return file.readAsString();
    } catch (e) {
      return null;
    }
  }

  //:::::::::::::::::::::::::: ADD LIST :::::::::::::::::::::::::::::::::
  void _addToDO() {
    setState(() {
      //--- UPDATE
      Map<String, dynamic> newToDO = Map();

      newToDO["title"] = _toDoController.text;
      _toDoController.text = "";

      newToDO["ok"] = false;
      _toDoList.add(newToDO);

      _saveData();
    });
  }

  //::::::::::::::::::::::::::: REFRESH :::::::::::::::::::::::::::::::::
  Future<Null> _refresh() async{
    //------- WAIT 1 SEC
    await Future.delayed(Duration(seconds: 1));

    setState(() {
      //------- NEXT ORDER LIST
      _toDoList.sort((a,b){
        if(a["ok"] && !b["ok"]) return 1;
        else if(!a["ok"] && b["ok"]) return -1;
        else return 0;
      });

      _saveData();
    });

    return Null; 
  }

  //::::::::::::::::::::::::::::: WIDGET LAYOUT ::::::::::::::::::::::::::::::::
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //------------------------- APP BAR ----------------
      appBar: AppBar(
        title: Text("Lista de Tarefas"),
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
      ),

      //------------------------- BODY --------------------
      body: Column(
        children: <Widget>[
          //:::::::::::::::::: CONTAINER (LINEAR LAYOUT - HORIZONTAL) :::::::::
          Container(
            // SPACE ---> PADDING
            padding: EdgeInsets.fromLTRB(17.0, 1.0, 7.0, 1.0),

            child: Row(
              // LINE (Horizontal)
              children: <Widget>[
                Expanded(
                  //--------------- SIZE
                  child: //--------------- TEXT FIELD ----
                      TextField(
                    controller: _toDoController,
                    decoration: InputDecoration(
                        labelText: "Nova Tarefa",
                        labelStyle: TextStyle(color: Colors.blueAccent)),
                  ),
                ),

                //----------------- Button
                RaisedButton(
                    color: Colors.blueAccent,
                    child: Text("ADD"),
                    textColor: Colors.white,
                    onPressed: _addToDO)
              ],
            ),
          ),

          //:::::::::::::::::::: LIST VIEW ::::::::::::::::::::::
          Expanded(
            //---------------- SIZE ---> PADDING
            child: RefreshIndicator(
              onRefresh: _refresh,
              child: ListView.builder(   // ADAPTER IN LIST VIEW
                padding: EdgeInsets.only(top: 10.0),
                itemCount: _toDoList.length,
                itemBuilder: buildItem,
              ),
            )

          )

        ],
      ),
    );
  }

  //::::::::::::::::::::::::::::::::: WIDGET BUILD ITEM ::::::::::::::::::::::::
  Widget buildItem(BuildContext context, int index) {
    //return ListTile(
    return Dismissible(
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),

      //------------------------------------
      background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment(-0.9, 0.0),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
      ),

      //------------------------------------
      direction: DismissDirection.startToEnd,

      //---------------------------------------
      child: CheckboxListTile(
        title: Text(_toDoList[index]["title"]),
        value: _toDoList[index]["ok"],
        secondary: CircleAvatar(
            child: Icon(_toDoList[index]["ok"] ? Icons.check : Icons.error)),
        onChanged: (c) {
          setState(() {
            _toDoList[index]["ok"] = c;
            _saveData();
          });
        },
      ),

      //-----------------------------------
      onDismissed: (direction) {
        setState(() {
          _lastRemoved = Map.from(_toDoList[index]);
          _lastRemovedPos = index;

          _toDoList.removeAt(index);
          _saveData();

          //:::::::::::::::::::::: CREATE SNACK BAR ::::::::::::::::::::
          final SnackBar snackBar = SnackBar(
            //-----------------------
            content: Text("Tarefa \"${_lastRemoved["title"]}\" removida!"),

            //-----------------------
            action: SnackBarAction(
              label: "Desfazer",
              onPressed: () {
                setState(() {
                  _toDoList.insert(_lastRemovedPos, _lastRemoved);
                  _saveData();
                });
              },
            ),

            //-------------------------
            duration: Duration(seconds: 2),
          );

          //:::::::::::::::::::::: SHOW SNACK BAR :::::::::::::::::::::
          Scaffold.of(context).showSnackBar(snackBar);
        });
      },

    );
  }
}
//******************************************************************************
